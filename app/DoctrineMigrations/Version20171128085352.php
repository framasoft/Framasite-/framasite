<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171128085352 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE domain DROP CONSTRAINT fk_a7a91e0bff956b22');
        $this->addSql('DROP SEQUENCE operation_id_seq CASCADE');
        $this->addSql('DROP TABLE operation');
        $this->addSql('DROP INDEX uniq_a7a91e0bff956b22');
        $this->addSql('ALTER TABLE domain ADD last_gandi_operation VARCHAR(255)');
        $this->addSql('ALTER TABLE domain DROP last_gandi_operation_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE operation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE operation (id INT NOT NULL, user_id INT DEFAULT NULL, gandi_id INT NOT NULL, eta INT NOT NULL, status INT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_1981a66da76ed395 ON operation (user_id)');
        $this->addSql('ALTER TABLE operation ADD CONSTRAINT fk_1981a66da76ed395 FOREIGN KEY (user_id) REFERENCES framasite_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE domain ADD last_gandi_operation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE domain DROP last_gandi_operation');
        $this->addSql('ALTER TABLE domain ADD CONSTRAINT fk_a7a91e0bff956b22 FOREIGN KEY (last_gandi_operation_id) REFERENCES operation (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_a7a91e0bff956b22 ON domain (last_gandi_operation_id)');
    }
}
