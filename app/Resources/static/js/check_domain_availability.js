import $ from 'jquery';

export default function (domain) {
    return $.post('/domain/attach/check', {
        domain: domain,
    });
}