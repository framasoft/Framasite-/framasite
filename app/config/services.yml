# Learn more about services, parameters and containers at
# https://symfony.com/doc/current/service_container.html
parameters:
    #parameter_name: value

services:
    # default configuration for services in *this* file
    _defaults:
        # automatically injects dependencies in your services
        autowire: true
        # automatically registers your services as commands, event subscribers, etc.
        autoconfigure: true
        # this means you cannot fetch services directly from the container via $container->get()
        # if you need to do this, you can override this setting on individual services
        public: false

    # makes classes in src/AppBundle available to be used as services
    # this creates a service per class whose id is the fully-qualified class name
    AppBundle\:
        resource: '../../src/AppBundle/*'
        # you can exclude directories or files
        # but if a service is unused, it's removed anyway
        exclude: '../../src/AppBundle/{Entity,Repository,Tests}'

    # controllers are imported separately to make sure they're public
    # and have a tag that allows actions to type-hint services
    AppBundle\Controller\:
        resource: '../../src/AppBundle/Controller'
        public: true
        tags: ['controller.service_arguments']

    UserBundle\Controller\:
        resource: '../../src/UserBundle/Controller'
        public: true
        tags: ['controller.service_arguments']

    # used for tests
    filesystem_cache:
        class: Doctrine\Common\Cache\FilesystemCache
        arguments:
            - "%kernel.cache_dir%/doctrine/metadata"

    # add more services, or override services that need manual wiring
    # AppBundle\Service\ExampleService:
    #     arguments:
    #         $someArgument: 'some_value'
    AppBundle\Async\AsyncAccountGrav:
        arguments:
            $accountsPath: '%app.path_blog_accounts%'
            $enginePath: '%app.path_grav%'

    AppBundle\Async\AsyncAccountDoku:
        arguments:
            $accountsPath: '%app.path_wiki_accounts%'
            $enginePath: '%app.path_doku%'

    AppBundle\Async\AsyncAccountPrettyNoemie:
        arguments:
            $accountsPath: '%app.path_simple_page_accounts%'
            $enginePath: '%app.path_pretty_noemie%'

    AppBundle\Async\AsyncDNSUpdate:
        arguments:
            - "@logger"
            - "@doctrine.orm.entity_manager"
            - '@AppBundle\Helper\ZoneFactory'
            - "%app.gandi.default_zone%"

    twig.extension.date:
         class: Twig_Extensions_Extension_Date
         arguments: ["@translator"]
         tags:
              - { name: twig.extension }

    twig.extension.intl:
        class: Twig_Extensions_Extension_Intl
        tags:
            - { name: twig.extension }

    AppBundle\Helper\DomainFactory:
        autowire: true
        public: true
        arguments:
          - "@logger"
          - '@AppBundle\Helper\ZoneFactory'
          - "%app.gandi.api.mode%"
          - "%app.gandi.api_key.prod%"
          - "%app.gandi.api_key.test%"

    AppBundle\Helper\ZoneFactory:
        autowire: true
        public: true
        arguments:
          - "@logger"
          - '%app.gandi.default_zone%'
          - "%app.gandi.api.mode%"
          - "%app.gandi.api_key.prod%"
          - "%app.gandi.api_key.test%"

    AppBundle\Validator\Constraints\ConfigurationDomainValidator:
        arguments:
          - "@logger"
          - "%app.gandi.api.mode%"
          - "%app.gandi.api_key.prod%"
          - "%app.gandi.api_key.test%"

    AppBundle\Validator\Constraints\ConfigurationGandiAccountValidator:
        arguments:
          - "@logger"
          - "%app.gandi.api.mode%"
          - "%app.gandi.api_key.prod%"
          - "%app.gandi.api_key.test%"

    AppBundle\Helper\SiteFactory:
        arguments:
          $blogAccountsPath: '%app.path_blog_accounts%'
          $wikiAccountsPath: '%app.path_wiki_accounts%'
          $singlePageAccountsPath: '%app.path_simple_page_accounts%'

    AppBundle\Helper\CertTaskFactory:
        arguments:
          - "@logger"
          - "@doctrine.orm.entity_manager"
          - "@doctrine.dbal.default_connection"

    framasites.exception.api:
        class: AppBundle\Event\Listener\APIExceptionListener
        arguments:
          - "@twig"
          - "@logger"
        tags:
          - { name: kernel.event_listener, event: kernel.exception }

    AppBundle\Helper\EmailFactory:
        arguments:
          - "@logger"
          - "%app.gandi.api.mode%"
          - "%app.gandi.api_key.prod%"
          - "%app.gandi.api_key.test%"

    AppBundle\Helper\UserFactory:
        arguments:
          - "@doctrine.orm.default_entity_manager"
          - "@logger"
          - '@AppBundle\Helper\DomainFactory'
          - '@AppBundle\Async\AsyncAccountGrav'
          - '@AppBundle\Async\AsyncAccountDoku'
          - '@AppBundle\Helper\CertTaskFactory'

    AppBundle\Service\Email\InvoiceService:
        arguments:
            $fromEmail: '%app.framasoft.email%'
            $framasoftAddress: '%app.framasoft.address%'

    AppBundle\Async\Gandi\CreateContactConsumer:
        arguments:
            $producer: '@check_contact_service'
            $serializer: '@jms_serializer'

    AppBundle\Async\Gandi\CheckContactConsumer:
        arguments:
            $producer: '@create_domain_service'
            $serializer: '@jms_serializer'

    AppBundle\Async\Gandi\CreateDomainConsumer:
        arguments:
            $serializer: '@jms_serializer'

    AppBundle\Service\Domain\RegisterDomainService:
        arguments:
            $serializer: "@jms_serializer"
            $producer: "@check_domain_service"
            $gandiMode: "%gandi_api_mode%"
            $gandiIdProd: "%gandi_id_prod%"
            $gandiIdTest: "%gandi_id_test%"

    AppBundle\Service\StripeKeyService:
        arguments:
            - "%stripe_api_mode%"
            - "%stripe_api_key_test_private%"
            - "%stripe_api_key_test_public%"
            - "%stripe_api_key_prod_private%"
            - "%stripe_api_key_prod_public%"
