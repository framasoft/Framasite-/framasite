<?php

namespace AppBundle\Form\SinglePage;

use AppBundle\Entity\SinglePage\SinglePageUser;
use AppBundle\Form\AbstractUserType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BlogUserType
 * @package AppBundle\Form\Blog
 */
class SinglePageUserType extends AbstractUserType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        // $choices = $options['lang_choices'];
        $builder
            /*->add('locale', ChoiceType::class, [
                'label' => 'site.new.user.locale',
                'choices' => $choices,
                'preferred_choices' => ['fr', 'en'],
                'label_attr' => ['class' => 'col-sm-2'],
                'data' => $options['user']->getLocale(),
            ])*/
            ->remove('email')
            ->remove('fullName')
            ->remove('admin')
            ->add('submit', SubmitType::class, [
                'label' => 'site.new.user.create',
                'attr' => ['class' => 'btn-success'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => SinglePageUser::class,
            'lang_choices' => null,
            'user' => null,
       ]);
    }
}
