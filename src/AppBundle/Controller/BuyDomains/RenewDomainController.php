<?php

namespace AppBundle\Controller\BuyDomains;

use AppBundle\Entity\Command;
use AppBundle\Entity\Domain;
use AppBundle\Exception\APIException;
use AppBundle\Exception\APIException\Renew\APIIssueRenewException;
use AppBundle\Exception\APIException\Renew\DomainNotOKRenewException;
use AppBundle\Exception\APIException\Renew\NotReadyRenewException;
use AppBundle\Helper\DomainFactory;
use AppBundle\Service\Domain\RenewDomainService;
use Doctrine\Common\Collections\ArrayCollection;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Translation\TranslatorInterface;

class RenewDomainController extends Controller
{
    /**
     * @var DomainFactory
     */
    private $domainFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * @var RenewDomainService
     */
    private $renewDomainService;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(
        LoggerInterface $logger,
                                FlashBagInterface $flashBag,
                                TranslatorInterface $translator,
                                DomainFactory $domainFactory,
                                RenewDomainService $renewDomainService
    ) {
        $this->logger = $logger;
        $this->flashBag = $flashBag;
        $this->translator = $translator;
        $this->domainFactory = $domainFactory;
        $this->renewDomainService = $renewDomainService;
    }

    /**
     * @Route("/domain/renew/{domain}", name="domain-renew", requirements={"domain": "\d+"})
     *
     * @param Domain $domain
     * @return Response
     * @throws \Exception
     */
    public function renewDomainAction(Domain $domain): Response
    {
        $this->checkAuthForDomain($domain);
        $this->logger->debug("I'm here");

        try {
            /** @var RenewDomainService $renewDomainService */
            $command = $this->renewDomainService->renewDomain($domain, $this->getUser(), $this->getParameter('app.framasoft.marge'));

            return $this->redirectToRoute('command-view', [
                'command' => $command->getId(),
            ]);
        } catch (DomainNotOKRenewException $e) {
            /**
             * If domain is not OK
             */
            $this->logger->error('User ' . $this->getUser()->getUsername() . " tried to renew the domain " . $domain->getDomainName() . " that hasn't got the status OK. Status was ". $domain->getStatus());
            $this->flashBag->add(
                'danger',
                $this->translator->trans('flashes.domain.renew.not_ready_deleted', [], 'messages')
            );
            return $this->redirectToRoute('domain-view', ['domain' => $domain->getId()]);
        } catch (APIIssueRenewException $e) {
            /**
             * If the BuyDomains API didn't respond
             */
            $this->logger->error('User ' . $this->getUser()->getUsername() . " tried to renew the domain " . $domain->getDomainName() . " but we didn't manage to get informations on the domain from GandiAPI");
            $this->logger->error('The error was ' . $e->getMessage());

            $this->flashBag->add(
                'danger',
                $this->translator->trans(
                    'flashes.domain.renew.info.error',
                    ['%domain%' => $domain->getDomainName(), '%error%' => $e->getMessage()],
                    'messages'
                )
            );
            return $this->redirectToRoute('domain-view', ['domain' => $domain->getId()]);
        } catch (NotReadyRenewException $e) {
            /**
             * The domain is not yet ready for renewal
             */
            $this->logger->warning('User ' . $this->getUser()->getUsername() . " tried to renew the domain " . $domain->getDomainName() . " but this domain isn't ready yet to be renewed. The date to renew starts on " . $e->getRenewDate());

            $this->flashBag->add(
                'danger',
                $this->translator->trans(
                    'flashes.domain.renew.not_yet',
                    ['%domain%' => $domain->getDomainName()],
                    'messages'
                )
            );
            return $this->redirectToRoute('domain-view', ['domain' => $domain->getId()]);
        }
    }

    /**
     * @Route("/domain/renew/confirm/{command}", name="domain-renew-confirm", requirements={"command": "\d+"})
     *
     * @param Command $command
     * @return Response
     */
    public function renewDomainConfirmAction(Command $command): Response
    {
        /** @var ArrayCollection $domains */
        $domains = $command->getDomains();
        foreach ($domains as $domain) {
            /** @var $domain Domain */
            try {
                $renewActionData = $this->domainFactory->renewDomain($domain);
                $this->logger->info('Domain renewed');

                $em = $this->getDoctrine()->getManager();

                $domain->setExpiresAt((new \DateTime())->add(new \DateInterval('P1Y')));
                $em->persist($domain);
                $em->flush();
            } catch (APIException $e) {
                $this->logger->error("Domain " . $domain->getDomainName() . " couldn't be renewed");
                $this->flashBag->add(
                    'danger',
                    $this->translator->trans(
                        'flashes.domain.renew.not_yet',
                        ['%error%' => $e->getMessage()],
                        'messages'
                    )
                );
            }
        }
        return $this->redirectToRoute('domain-view', ['domain' => $domains->first()]);
    }

    /**
     * @param Domain $domain
     * @return bool
     */
    private function checkAuthForDomain(Domain $domain)
    {
        if ($domain->getUser() !== $this->getUser() && !($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->hasRole('SUPER_ADMIN'))) {
            throw new AccessDeniedException("You don't have access to this domain");
        }
        return true;
    }
}
