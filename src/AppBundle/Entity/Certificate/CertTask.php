<?php

namespace AppBundle\Entity\Certificate;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class CertTask
{
    const STATUS_TODO = 10;
    const STATUS_WAITING_DNS = 20;
    const STATUS_WAITING = 30;
    const STATUS_FINISHED = 42;
    const STATUS_FAILED = 1337;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @Assert\Choice({ 10, 20, 42, 1337 })
     */
    private $status;

    /**
     * @var CertData
     *
     * @ORM\Column(type="json_document", options={"jsonb": false})
     */
    private $taskData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="last_updated")
     */
    private $lastUpdated;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="certTasks")
     */
    private $user;

    /**
     * CertTask constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->lastUpdated = new \DateTime();
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return CertTask
     */
    public function setStatus(int $status): CertTask
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return CertData
     */
    public function getTaskData(): CertData
    {
        return $this->taskData;
    }

    /**
     * @param CertData $taskData
     * @return CertTask
     */
    public function setTaskData(CertData $taskData): CertTask
    {
        $this->taskData = $taskData;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdated(): \DateTime
    {
        return $this->lastUpdated;
    }

    /**
     * @param \DateTime $lastUpdated
     * @return CertTask
     */
    public function setLastUpdated(\DateTime $lastUpdated): CertTask
    {
        $this->lastUpdated = $lastUpdated;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return CertTask
     */
    public function setUser(User $user = null): CertTask
    {
        $this->user = $user;
        return $this;
    }
}
