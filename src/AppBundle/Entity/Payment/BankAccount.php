<?php

namespace AppBundle\Entity\Payment;

class BankAccount
{

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $iban;

    /**
     * @var string
     */
    private $bic;

    /**
     * @var string
     */
    private $ownerName;

    /**
     * @var string
     */
    private $ownerAddress;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return BankAccount
     */
    public function setId(string $id): BankAccount
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return BankAccount
     */
    public function setUserId(string $userId): BankAccount
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     * @return BankAccount
     */
    public function setIban(string $iban): BankAccount
    {
        $this->iban = $iban;
        return $this;
    }

    /**
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * @param string $bic
     * @return BankAccount
     */
    public function setBic(string $bic): BankAccount
    {
        $this->bic = $bic;
        return $this;
    }

    /**
     * @return string
     */
    public function getOwnerName()
    {
        return $this->ownerName;
    }

    /**
     * @param string $ownerName
     * @return BankAccount
     */
    public function setOwnerName(string $ownerName): BankAccount
    {
        $this->ownerName = $ownerName;
        return $this;
    }

    /**
     * @return string
     */
    public function getOwnerAddress()
    {
        return $this->ownerAddress;
    }

    /**
     * @param string $ownerAddress
     * @return BankAccount
     */
    public function setOwnerAddress(string $ownerAddress): BankAccount
    {
        $this->ownerAddress = $ownerAddress;
        return $this;
    }
}
