<?php

namespace AppBundle\Entity\Zone;

use Doctrine\Common\Collections\ArrayCollection;

class Version
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var ArrayCollection
     */
    private $records;

    /**
     * @var Zone
     */
    private $zone;

    /**
     * @var Version
     */
    private $parent;

    /**
     * Version constructor.
     * @param Zone $zone
     * @param Version|null $parent
     */
    public function __construct(Zone $zone, Version $parent = null)
    {
        $this->zone = $zone;
        $this->records = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Version
     */
    public function setId(int $id): Version
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRecords(): ArrayCollection
    {
        return $this->records;
    }

    /**
     * @param ArrayCollection $records
     * @return Version
     */
    public function setRecords(ArrayCollection $records): Version
    {
        $this->records = $records;
        return $this;
    }

    /**
     * @param Record $record
     * @return Version
     */
    public function addRecord(Record $record): Version
    {
        $this->records->add($record);
        return $this;
    }

    /**
     * @return Zone
     */
    public function getZone(): Zone
    {
        return $this->zone;
    }

    /**
     * @param Zone $zone
     * @return Version
     */
    public function setZone(Zone $zone): Version
    {
        $this->zone = $zone;
        return $this;
    }

    /**
     * @return Version
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Version $parent
     * @return Version
     */
    public function setParent(Version $parent): Version
    {
        $this->parent = $parent;
        return $this;
    }
}
