<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Blog\Blog;
use AppBundle\Entity\Certificate\CertTask;
use AppBundle\Entity\Wiki\Wiki;
use AppBundle\Entity\SinglePage\SinglePage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use AppBundle\Validator\Constraints as FramasitesAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="framasite_user")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"domain_register"})
     */
    protected $id;

    /**
     * @Groups({"domain_register"})
     */
    protected $username;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="fos_user.email.blank")
     * @Assert\Email(message="fos_user.email.invalid")
     * TODO : Remove me soon !
     * //Assert\Regex(pattern="/framasoft.org$/", message="Seuls les membres de Framasoft (avec leur adresse @framasoft.org) peuvent s'inscrire pour le moment !")
     */
    protected $email;

    /**
     * @var string
     * @Assert\Length(min=8)
     */
    protected $plainPassword;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="Site", mappedBy="user")
     */
    protected $sites;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Domain", mappedBy="user")
     */
    protected $domains;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Command", mappedBy="user")
     */
    protected $commands;

    /**
     * @var Command
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Command")
     */
    protected $activeCommand = null;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Certificate\CertTask", mappedBy="user")
     */
    protected $certTasks;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Payment\Card", mappedBy="user")
     */
    protected $cards;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     */
    protected $type = 1;

    const USER_TYPE_NATURAL = 1;
    const USER_TYPE_LEGAL_BUSINESS = 2;
    const USER_TYPE_LEGAL_ORGANIZATION = 3;
    const USER_TYPE_LEGAL_SOLETRADER = 4;

    /**
     * @var string
     *
     * @ORM\Column(name="legal_name", type="string", nullable=true)
     */
    protected $legalName;

    /**
     * @var string
     *
     * @Assert\Locale()
     * @ORM\Column(name="locale", type="string")
     */
    protected $locale = 'fr';

    /**
     * @var string
     *
     * @Assert\Country()
     * @ORM\Column(name="country", type="string", nullable=true)
     */
    protected $country = 'FR';

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", nullable=true)
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", nullable=true)
     */
    protected $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="street_address", type="string", nullable=true)
     */
    private $streetAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", nullable=true)
     */
    private $zipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", nullable=true)
     */
    private $city;

    /**
     * @var \DateTime
     *
     * @Assert\DateTime()
     * @ORM\Column(name="birthday", type="datetimetz", nullable=true)
     */
    protected $birthday;

    /**
     * @var string
     *
     * @FramasitesAssert\ConstraintValidGandiAccountConfiguration()
     * @ORM\Column(name="gandi_id", type="string", nullable=true)
     */
    protected $gandiId;

    /**
     * @var string
     *
     * @ORM\Column("stripe_id", type="string", nullable=true)
     */
    private $stripeId;

    public function __construct()
    {
        parent::__construct();
        $this->sites = new ArrayCollection();
        $this->domains = new ArrayCollection();
        $this->commands = new ArrayCollection();
        $this->certTasks = new ArrayCollection();
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt(\DateTime $createdAt): User
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt(\DateTime $updatedAt): User
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function timestamps()
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }

        $this->updatedAt = new \DateTime();
    }

    /**
     * @return ArrayCollection
     */
    public function getSites()
    {
        return $this->sites;
    }

    /**
     * @param mixed $sites
     * @return User
     */
    public function setSites($sites): User
    {
        $this->sites = $sites;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getBlogs()
    {
        foreach ($this->sites as $site) {
            if ($site instanceof Blog) {
                return new ArrayCollection([$site]);
            }
        }
        return new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getWikis()
    {
        foreach ($this->sites as $site) {
            if ($site instanceof Wiki) {
                return new ArrayCollection([$site]);
            }
        }
        return new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getSinglePages()
    {
        foreach ($this->sites as $site) {
            if ($site instanceof SinglePage) {
                return new ArrayCollection([$site]);
            }
        }
        return new ArrayCollection();
    }

    /**
     * @return Collection
     */
    public function getDomains(): Collection
    {
        return $this->domains;
    }

    /**
     * @param Collection $domains
     * @return User
     */
    public function setDomains(Collection $domains): User
    {
        $this->domains = $domains;
        return $this;
    }

    /**
     * @param Domain $domain
     * @return User
     */
    public function addDomain(Domain $domain): User
    {
        $this->domains->add($domain);
        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return User
     */
    public function setLocale(string $locale): User
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommands()
    {
        return $this->commands;
    }

    /**
     * @param mixed $commands
     * @return User
     */
    public function setCommands($commands): User
    {
        $this->commands = $commands;
        return $this;
    }

    /**
     * @param Command $command
     * @return User
     */
    public function addCommand(Command $command): User
    {
        $this->commands->add($command);
        return $this;
    }

    /**
     * @return Command
     */
    public function getActiveCommand()
    {
        return $this->activeCommand;
    }

    /**
     * @param Command $activeCommand
     * @return User
     */
    public function setActiveCommand(Command $activeCommand = null): User
    {
        $this->activeCommand = $activeCommand;
        return $this;
    }

    /**
     * @return string
     */
    public function getGandiId()
    {
        return $this->gandiId;
    }

    /**
     * @param string $gandiId
     * @return User
     */
    public function setGandiId($gandiId): User
    {
        $this->gandiId = $gandiId;
        return $this;
    }

    /**
     * @return User
     */
    public function removeGandiId(): User
    {
        $this->gandiId = null;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCertTasks()
    {
        return $this->certTasks;
    }

    /**
     * @param Collection $certTasks
     * @return User
     */
    public function setCertTasks(Collection $certTasks): User
    {
        $this->certTasks = $certTasks;
        return $this;
    }

    /**
     * Clear all cert tasks for user
     *
     * @ORM\PreRemove()
     * @return User
     */
    public function clearCertTasks(): User
    {
        foreach ($this->certTasks as $certTask) {
            /** @var CertTask $certTask */
            $certTask->setUser(null);
        }
        $this->certTasks = new ArrayCollection();
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return User
     */
    public function setCity(string $city = null): User
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }

    /**
     * @param string $streetAddress
     * @return User
     */
    public function setStreetAddress(string $streetAddress = null): User
    {
        $this->streetAddress = $streetAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     * @return User
     */
    public function setZipCode(string $zipCode = null): User
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return User
     */
    public function setFirstName(string $firstName = null): User
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return User
     */
    public function setCountry(string $country = null): User
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return User
     */
    public function setLastName(string $lastName = null): User
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday
     * @return User
     */
    public function setBirthday(\DateTime $birthday = null): User
    {
        $this->birthday = $birthday;
        return $this;
    }

    /**
     * @return string
     */
    public function getLegalName()
    {
        return $this->legalName;
    }

    /**
     * @param string $legalName
     * @return User
     */
    public function setLegalName(string $legalName = null): User
    {
        $this->legalName = $legalName;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return User
     */
    public function setType(int $type): User
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getStripeId()
    {
        return $this->stripeId;
    }

    /**
     * @param string $stripeId
     * @return User
     */
    public function setStripeId(string $stripeId): User
    {
        $this->stripeId = $stripeId;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCards()
    {
        return $this->cards;
    }

    /**
     * @param Collection $cards
     * @return User
     */
    public function setCards(Collection $cards): User
    {
        $this->cards = $cards;
        return $this;
    }
}
