<?php

namespace AppBundle\Entity\Blog;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Site;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class Blog extends Site
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    protected $siteName;

    /**
     * @var string
     */
    protected $siteDescription;

    /**
     * @var string
     */
    protected $siteKeywords;

    /**
     * @var array
     */
    protected $siteModules;

    const MODULE_BLOG = 1;
    const MODULE_CV = 2;
    const MODULE_SINGLE_PAGE = 3;
    const MODULE_SINGLE_PAGE_CARROUSEL = 4;

    /**
     * @var int
     */
    protected $homepage;

    /**
     * @return string
     */
    public function getSiteName()
    {
        return $this->siteName;
    }

    /**
     * @param string $siteName
     * @return Blog
     */
    public function setSiteName(string $siteName): Blog
    {
        $this->siteName = $siteName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSiteDescription()
    {
        return $this->siteDescription;
    }

    /**
     * @param string $siteDescription
     * @return Blog
     */
    public function setSiteDescription(string $siteDescription): Blog
    {
        $this->siteDescription = $siteDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getSiteKeywords()
    {
        return $this->siteKeywords;
    }

    /**
     * @param string $siteKeywords
     * @return Blog
     */
    public function setSiteKeywords(string $siteKeywords): Blog
    {
        $this->siteKeywords = $siteKeywords;
        return $this;
    }

    /**
     * @return array
     */
    public function getSiteModules()
    {
        return $this->siteModules;
    }

    /**
     * @param array $siteModules
     * @return Blog
     */
    public function setSiteModules(array $siteModules): Blog
    {
        $this->siteModules = $siteModules;
        return $this;
    }

    /**
     * @return int
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * @param int $homepage
     * @return Blog
     */
    public function setHomepage(int $homepage): Blog
    {
        $this->homepage = $homepage;
        return $this;
    }

    public function __toString()
    {
        return $this->subdomain . '.frama.site';
    }
}
