<?php

namespace AppBundle\Exception;

use Exception;
use Symfony\Component\Security\Core\User\UserInterface;

class SubDomainUnavailableException extends Exception
{

    /** @var int */
    private $reason;

    /** @var string */
    private $subDomain;

    /** @var UserInterface */
    private $user;

    const REASON_FORBIDDEN = 1;
    const REASON_TAKEN = 2;

    /**
     * SubDomainUnavailableException constructor.
     * @param int $reason
     * @param string $subDomain
     * @param UserInterface $user
     * @param string $message
     */
    public function __construct(int $reason, string $subDomain, UserInterface $user, $message = "")
    {
        parent::__construct($message);
        $this->reason = $reason;
        $this->subDomain = $subDomain;
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getSubDomain(): string
    {
        return $this->subDomain;
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getReason(): int
    {
        return $this->reason;
    }
}
