<?php

namespace AppBundle\Helper;

use AppBundle\Async\AsyncAccountDoku;
use AppBundle\Async\AsyncAccountGrav;
use AppBundle\Entity\Blog\Blog;
use AppBundle\Entity\Certificate\CertData;
use AppBundle\Entity\Certificate\CertTask;
use AppBundle\Entity\Domain;
use AppBundle\Entity\Site;
use AppBundle\Entity\User;
use AppBundle\Entity\Wiki\Wiki;
use AppBundle\Exception\APIException;
use AppBundle\Exception\SiteException\SiteDeletionException;
use AppBundle\Exception\UnknownSiteType;
use AppBundle\Exception\UserDeletionException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class UserFactory
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var DomainFactory
     */
    private $domainFactory;

    /**
     * @var AsyncAccountGrav
     */
    private $accountGrav;

    /**
     * @var AsyncAccountDoku
     */
    private $accountDoku;

    /**
     * @var CertTaskFactory
     */
    private $certTaskFactory;

    public function __construct(
        EntityManagerInterface $entityManager,
                                LoggerInterface $logger,
                                DomainFactory $domainFactory,
                                AsyncAccountGrav $accountGrav,
                                AsyncAccountDoku $accountDoku,
                                CertTaskFactory $certTaskFactory
                                ) {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->domainFactory = $domainFactory;
        $this->accountGrav = $accountGrav;
        $this->accountDoku = $accountDoku;
        $this->certTaskFactory = $certTaskFactory;
    }

    /**
     * @param User $user
     * @throws UserDeletionException
     */
    public function deleteDependencies(User $user)
    {
        /**
         * Remove all websites and attached domains
         */
        foreach ($user->getSites() as $site) {
            try {
                $this->deleteSiteAndAttachedDomains($site, $user);
            } catch (SiteDeletionException $e) {
                $this->logger->error("Error while deleting site : " . $e->getMessage());
                throw new UserDeletionException();
            }
        }

        /**
         * Remove domains which are not attached
         */
        foreach ($user->getDomains() as $domain) {
            /** @var $domain Domain */
            $this->detachDomain($domain, $user);
            $this->entityManager->remove($domain);
        }

        /**
         * Release contact from our account
         */
        if ($user->getGandiId()) {
            try {
                $this->domainFactory->releaseContact($user->getGandiId());
            } catch (APIException $e) {
                $this->logger->error("Error while releasing contact : " . $e->getMessage());
                throw new UserDeletionException();
            }
        }

        /**
         * Detach cert_tasks from this account
         */
        foreach ($user->getCertTasks() as $certTask) {
            /** @var CertTask $certTask */
            $certTask->setUser(null);
            $this->entityManager->persist($certTask);
        }
        $user->clearCertTasks();
        $this->entityManager->persist($user);

        $this->entityManager->flush();
    }

    /**
     * @param Site $site
     * @param User $user
     * @throws \AppBundle\Exception\SiteException\SiteDeletionException
     */
    private function deleteSiteAndAttachedDomains(Site $site, User $user)
    {
        /**
         * Let's start with resetting domains sites
         */
        foreach ($site->getDomains() as $domain) {
            /** @var $domain Domain */
            $this->detachDomain($domain, $user);
            $domain->removeSite();
            $this->entityManager->remove($domain);
        }

        /**
         * Delete files
         */
        if ($site instanceof Blog) {
            $this->accountGrav->setSite($site)->setLogger($this->logger);
            $this->accountGrav->deleteSite();
        } elseif ($site instanceof Wiki) {
            $this->accountDoku->setSite($site)->setLogger($this->logger);
            $this->accountDoku->deleteSite();
        }

        /**
         * Delete site in database
         */
        $this->entityManager->remove($site);
        $this->entityManager->flush();
    }

    /**
     * @param Domain $domain
     * @param User $user
     */
    private function detachDomain(Domain $domain, User $user)
    {
        try {
            $this->certTaskFactory->createCertTaskForAttachment($user, $domain, CertData::ACTION_DELETE);
        } catch (UnknownSiteType $e) {
            $this->logger->error("Couldn't create cert task : Site is of unknown type : " . get_class($domain->getSite()));
        }
        if ($domain->isRegisteredByFrama()) {
            // If the domain has been registered with us, we have to release the domain
            try {
                $this->logger->info("We registered the domain, so now we release it");
                $this->domainFactory->releaseDomain($domain);
            } catch (APIException $e) {
                $this->logger->error("Couldn't detach domain " . $domain->getDomainName() . ' error message : ' . $e->getMessage());
            }
        }
    }
}
