<?php

namespace AppBundle\Async;

use AppBundle\Entity\User;
use Psr\Log\LoggerAwareInterface;

interface AsyncInterface extends LoggerAwareInterface
{

    /**
     * Set User
     * @param User $user
     */
    public function setUser(User $user);
}
