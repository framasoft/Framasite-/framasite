<?php

namespace AppBundle\Async;

class Tools {


    /**
     * Creates a relative symlink from $link to $target
     *
     * @param  string $target The path of the binary file to be symlinked
     * @param  string $link   The path where the symlink should be created
     * @return bool
     */
    static function relativeSymlink($target, $link)
    {
        $cwd = getcwd();
        $relativePath = self::findShortestPath($link, $target);
        chdir(dirname($link));
        $result = @symlink($relativePath, $link);
        chdir($cwd);
        return (bool) $result;
    }

    /**
     * Returns the shortest path from $from to $to
     *
     * @param  string                    $from
     * @param  string                    $to
     * @param  bool                      $directories if true, the source/target are considered to be directories
     * @throws \InvalidArgumentException
     * @return string
     */
    static function findShortestPath($from, $to, $directories = false)
    {
        if (!self::isAbsolutePath($from) || !self::isAbsolutePath($to)) {
            throw new \InvalidArgumentException(sprintf('$from (%s) and $to (%s) must be absolute paths.', $from, $to));
        }
        $from = lcfirst(self::normalizePath($from));
        $to = lcfirst(self::normalizePath($to));
        if ($directories) {
            $from = rtrim($from, '/') . '/dummy_file';
        }
        if (dirname($from) === dirname($to)) {
            return './'.basename($to);
        }
        $commonPath = $to;
        while (strpos($from.'/', $commonPath.'/') !== 0 && '/' !== $commonPath && !preg_match('{^[a-z]:/?$}i', $commonPath)) {
            $commonPath = strtr(dirname($commonPath), '\\', '/');
        }
        if (0 !== strpos($from, $commonPath) || '/' === $commonPath) {
            return $to;
        }
        $commonPath = rtrim($commonPath, '/') . '/';
        $sourcePathDepth = substr_count(substr($from, strlen($commonPath)), '/');
        $commonPathCode = str_repeat('../', $sourcePathDepth);
        return ($commonPathCode . substr($to, strlen($commonPath))) ?: './';
    }

    /**
     * Checks if the given path is absolute
     *
     * @param  string $path
     * @return bool
     */
    static function isAbsolutePath($path)
    {
        return substr($path, 0, 1) === '/' || substr($path, 1, 1) === ':';
    }

    /**
     * Normalize a path. This replaces backslashes with slashes, removes ending
     * slash and collapses redundant separators and up-level references.
     *
     * @param  string $path Path to the file or directory
     * @return string
     */
    static function normalizePath($path)
    {
        $parts = array();
        $path = strtr($path, '\\', '/');
        $prefix = '';
        $absolute = false;
        // extract a prefix being a protocol://, protocol:, protocol://drive: or simply drive:
        if (preg_match('{^( [0-9a-z]{2,}+: (?: // (?: [a-z]: )? )? | [a-z]: )}ix', $path, $match)) {
            $prefix = $match[1];
            $path = substr($path, strlen($prefix));
        }
        if (substr($path, 0, 1) === '/') {
            $absolute = true;
            $path = substr($path, 1);
        }
        $up = false;
        foreach (explode('/', $path) as $chunk) {
            if ('..' === $chunk && ($absolute || $up)) {
                array_pop($parts);
                $up = !(empty($parts) || '..' === end($parts));
            } elseif ('.' !== $chunk && '' !== $chunk) {
                $parts[] = $chunk;
                $up = '..' !== $chunk;
            }
        }
        return $prefix.($absolute ? '/' : '').implode('/', $parts);
    }
}
