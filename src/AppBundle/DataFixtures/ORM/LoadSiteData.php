<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Async\AsyncAccountDoku;
use AppBundle\Async\AsyncAccountGrav;
use AppBundle\Entity\Blog\Blog;
use AppBundle\Entity\Blog\BlogUser;
use AppBundle\Entity\Wiki\Wiki;
use AppBundle\Entity\Wiki\WikiUser;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadSiteData extends Fixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        /** @var AsyncAccountGrav $accountGrav */
        $accountGrav = $this->container->get(AsyncAccountGrav::class);
        /** @var AsyncAccountDoku $accountDoku */
        $accountDoku = $this->container->get(AsyncAccountDoku::class);

        $site1 = new Blog($this->getReference('admin-user'));
        $site1->setSubdomain('toto');
        $site1->setCreatedAt(new \DateTime());
        $site1->setSiteName('Mon super site');
        $site1->setSiteDescription('This is my description /o/');
        $site1->setSiteKeywords('thomas, tutu');

        $site1->addDomain($this->getReference('foo-domain'));
        $site1->addDomain($this->getReference('baz-domain'));

        $manager->persist($site1);

        $siteUser1 = new BlogUser();
        $siteUser1->setUsername('toto');
        $accountGrav->setSite($site1)->setSiteUser($siteUser1)->setUser($this->getReference('admin-user'));
        $accountGrav->make();

        $this->addReference('site1', $site1);

        $site2 = new Wiki($this->getReference('admin-user'));
        $site2->setSubdomain('neo');
        $site2->setPolicy(1);
        $site2->setSiteName('Super wikis !');

        $manager->persist($site2);

        $siteUser2 = new WikiUser();
        $siteUser2->setUsername('neo');
        $siteUser2->setPassword('neopass');
        $accountDoku->setSite($site2)->setSiteUser($siteUser2)->setUser($this->getReference('admin-user'));
        $accountDoku->make();

        $this->addReference('site2', $site2);

        $site3 = new Blog($this->getReference('empty-user'));
        $site3->setSubdomain('toto2');
        $site3->setCreatedAt(new \DateTime());
        $site3->setSiteName('Mon super site 3');
        $site3->setSiteDescription('This is my description /o/');
        $site3->setSiteKeywords('thomas, tutu');

        $manager->persist($site3);

        $siteUser3 = new BlogUser();
        $siteUser3->setUsername('toto');
        $accountGrav->setSite($site3)->setSiteUser($siteUser3)->setUser($this->getReference('empty-user'));
        $accountGrav->make();

        $this->addReference('site3', $site3);

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 30;
    }
}
