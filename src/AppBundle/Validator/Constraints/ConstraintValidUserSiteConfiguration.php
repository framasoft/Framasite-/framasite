<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ConstraintValidUserSiteConfiguration extends Constraint
{
    public $alreadyExistsUserSiteMessage = 'site.user.already_exists';

    public function validatedBy()
    {
        return ConfigurationUserSiteValidator::class;
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
