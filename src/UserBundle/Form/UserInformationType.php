<?php

namespace UserBundle\Form;

use AppBundle\Entity\User;
use FOS\UserBundle\Form\Type\RegistrationFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserInformationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'settings.profile.type.user' => User::USER_TYPE_NATURAL,
                    'settings.profile.type.business' => User::USER_TYPE_LEGAL_BUSINESS,
                    'settings.profile.type.organization' => User::USER_TYPE_LEGAL_ORGANIZATION,
                ],
                'label' => 'settings.profile.type.label',
                'expanded' => true,
                'multiple' => false,
                'label_attr' => ['class' => 'col-sm-3 radio-inline'],
                'empty_data' => User::USER_TYPE_NATURAL,
                'required' => true,
            ])
            ->add('legalName', TextType::class, [
                'label' => 'settings.profile.legal_name.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['autocomplete' => 'organization'],
                'required' => false,
            ])
            ->add('email', EmailType::class, [
                'label' => 'settings.profile.email.label',
                'attr' => ['autocomplete' => 'email'],
                'label_attr' => ['class' => 'col-sm-3'],
            ])
            ->add('firstName', TextType::class, [
                'label' => 'settings.profile.first_name.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['autocomplete' => 'given-name'],
                'required' => false,
            ])
            ->add('lastName', TextType::class, [
                'label' => 'settings.profile.last_name.label',
                'label_attr' => ['class' => 'col-sm-2'],
                'attr' => ['autocomplete' => 'family-name'],
                'required' => false,
            ])
            ->add('birthday', BirthdayType::class, [
                'label' => 'settings.profile.birthday.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['autocomplete' => 'bday'],
                'required' => false,
                'years' => range(date('Y'), date('Y') - 120)
            ])
            ->add('streetAddress', TextareaType::class, [
                'label' => 'settings.profile.street_address.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['autocomplete' => 'street-address'],
                'required' => false,
            ])
            ->add('zipCode', TextType::class, [
                'label' => 'settings.profile.zip_code.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['autocomplete' => 'postal-code'],
                'required' => false,
            ])
            ->add('city', TextType::class, [
                'label' => 'settings.profile.city.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'attr' => ['autocomplete' => 'address-level2'],
                'required' => false,
            ])
            ->add('country', CountryType::class, [
                'label' => 'settings.profile.country.label',
                'label_attr' => ['class' => 'col-sm-3'],
                'required' => false,
                'attr' => ['autocomplete' => 'country'],
                'preferred_choices' => ['FR', 'GB', 'US', 'DE'],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'settings.save',
            ])
            ->remove('username')
            ->remove('plainPassword')
        ;
    }

    public function getParent()
    {
        return RegistrationFormType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => User::class,
       ]);
    }

    public function getBlockPrefix()
    {
        return 'update_user';
    }
}
